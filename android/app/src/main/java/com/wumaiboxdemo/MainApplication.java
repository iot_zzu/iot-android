package com.wumaiboxdemo;

import android.app.Application;

import com.facebook.react.ReactApplication;
import org.reactnative.camera.RNCameraPackage;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.devstepbcn.wifi.AndroidWifiPackage;
import com.tuanpm.RCTSmartconfig.RCTSmartconfigPackage;
import cn.qiuxiang.react.amap3d.AMap3DPackage;
import com.beefe.picker.PickerViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
//import com.reactnativecomponent.barcode.RCTCapturePackage;
import com.reactnativecomponent.amaplocation.RCTAMapLocationPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
  //private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNCameraPackage(),
            new RNNetworkInfoPackage(),
            new AndroidWifiPackage(),
            new RCTSmartconfigPackage(),
            new AMap3DPackage(),
            new PickerViewPackage(),
//            new RCTCapturePackage(),
            new RCTAMapLocationPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };
    public void setReactNativeHost(ReactNativeHost reactNativeHost) {
      mReactNativeHost = reactNativeHost;
    }

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
