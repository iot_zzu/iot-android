import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    CheckBox,
    TouchableHighlight,
    Platform,
    Alert,

} from 'react-native';
import { NetworkInfo } from 'react-native-network-info';
import Smart from 'react-native-smartconfig';

var wifi = require('react-native-android-wifi')


export default class MakeInternet extends Component{
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            modalVisible: false,
            isSmart: false,
            ssid: '',
            pass: '',
            btnLabel: '确定',

        };
        if (Platform.OS === 'android') {
            wifi.isEnabled((isEnabled) => {
                if (isEnabled) {
                    console.log("wifi service enabled");
                } else {
                    Alert.alert(
                        'Wifi is dissable',
                        'Do you want to enable Wifi?',
                        [
                            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            { text: 'OK', onPress: () => wifi.setEnabled(true) },
                        ],
                        { cancelable: false }
                    )
                }
            })
        }
        NetworkInfo.getSSID(ssid =>{
            this.setState({ssid:ssid})
        })
    }
    static navigationOptions = ({navigation, screenProps}) => ({

        headerTitle: '配置网络',
        //设置滑动返回的距离
        gestureResponseDistance: {horizontal: 300},

        //是否开启手势滑动返回，android 默认关闭 ios打开
        gesturesEnabled: true,

        //设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题
        headerBackTitle: null,
        //导航栏的样式
        headerStyle: styles.headerStyle,
        //导航栏文字的样式
        headerTitleStyle: styles.headerTitleStyle,
        //返回按钮的颜色
        headerTintColor: 'white',

        //隐藏顶部导航栏
        // header: null,

        //设置顶部导航栏右边的视图  和 解决当有返回箭头时，文字不居中
        headerRight: (<View/>),

        //设置导航栏左边的视图
        // headerLeft: (<View/>),

    });


    smartConfig = () => {
        Alert.alert('是否要连接我的设备');

        if (this.state.isSmart) {
            console.log('stop');
            Smart.stop();
            this.setState({ modalVisible: false, isSmart: false, btnLabel: 'Submit' });
            return;
        }
        console.log('start');
        this.setState({ modalVisible: true, isSmart: true, btnLabel: 'Cancel' });
        this.setState({ isSmart: true });
        var self = this;
        Smart.start({
            type: 'esptouch',
            ssid: this.state.ssid,
            bssid: '', //"" if not need to filter (don't use null)
            password: this.state.pass,
            timeout:5000
        }).then(function (results) {
            //Array of device success do smartconfig
            self.setState({ modalVisible: false, btnLabel: 'Submit', isSmart: false });
            var alertMsg = 'Devices was connected:\n';
            for (var i = 0; i < results.length; i++) {
                alertMsg += results[i].ipv4 + '\n';
            }
            Alert.alert(
                'Smartconfig',
                alertMsg
            );
        }).catch(function (error) {
            Alert.alert('连接失败');
        });
    }


    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.ssid !== this.state.ssid) {
            this.setState({ ssid: nextProps.ssid });
        }
    }

    render(){
        var enable = false;
        return(
            <View style={styles.container}>
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                    <View style={styles.container}>
                        {/*<View style={{ width: 300, height: 150, alignItems: 'center', justifyContent: 'center' }}>*/}
                        {/*<Image*/}
                        {/*style={{ width: 150, height: 77, margin: 10 }}*/}
                        {/*source={Logo}*/}
                        {/*/>*/}
                        {/*</View>*/}
                        <Text style={styles.text}>当前网络的SSID：{this.state.ssid}</Text>
                        <TextInput
                            style={styles.input}
                            placeholder='请输入WIFI名称'
                            autoCapitalize='none'
                            onChangeText={(text) => this.setState({ ssid: text })}
                            value={this.state.ssid}
                        />
                        <TextInput
                            secureTextEntry={true}
                            style={styles.input}
                            placeholder='请输入WIFI密码'
                            autoCapitalize='none'
                            onChangeText={(text) => this.setState({ pass: text })}
                            value={this.state.pass}
                        />
                        <TouchableHighlight
                            style={styles.submit}
                            onPress={this.smartConfig}
                            underlayColor='grey'
                        >
                            <Text style={styles.submitText}>
                                {this.state.btnLabel}
                            </Text>
                        </TouchableHighlight>
                    </View>
                    <View style={{ alignItems: 'center', backgroundColor: '#F5FCFF' }}>


                    </View>
                </View>
                {/*<View style={{width:'100%'}}>*/}
                    {/*<Text style={{fontSize:22,}}>当前网络的SSID:</Text>*/}
                    {/*<Text style={{fontSize:22,}}>请选择要配置的WiFi1</Text>*/}
                {/*</View>*/}
                {/*<View></View>*/}
                {/*<View style={styles.inputViewStyle}>*/}
                    {/*<View style={{width:'100%',height:30}}></View>*/}
                    {/*<TextInput style={styles.inputStyle} placeholder={'请输入WiFi密码'} underlineColorAndroid="transparent"/>*/}
                    {/*<View style={{width:'100%',height:30}}></View>*/}
                    {/*<TextInput style={styles.inputStyle} placeholder={'请输入WiFi名称'} underlineColorAndroid="transparent"/>*/}
                    {/*<View style={{width:'100%',height:30}}></View>*/}
                    {/*<TextInput style={styles.inputStyle} placeholder={'请输入WiFi密码'} underlineColorAndroid="transparent" />*/}
                    {/*<View style={{flexDirection:'row',alignSelf:'flex-start',width:'100%'}}>*/}
                        {/*<CheckBox /><Text style={{alignSelf:'center'}}>显示密码</Text>*/}
                    {/*</View>*/}
                    {/*<View style={{width:'100%',height:30}}></View>*/}
                    {/*<TouchableHighlight style={styles.button}>*/}
                        {/*<Text style={{alignSelf:'center',fontSize:20}}>确定</Text>*/}
                    {/*</TouchableHighlight>*/}
                {/*</View>*/}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        margin: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input: {
        margin: 5,
        height: 30,
        width: 200,
        borderRadius: 5,
        padding: 5,
        borderColor: 'grey',
        borderWidth: 1
    },
    inputViewStyle:{
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        width:'70%',
    },
    inputStyle:{
        width:'100%',
        borderWidth:1,
        borderRadius:5,
        borderColor:'#dbdbdb',
    },
    button:{
        width:'50%',
        height:40,
        borderRadius:5,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#2255FF'
    },
    headerStyle: {
        backgroundColor: '#292D33',
    },
    headerTitleStyle: {
        color: 'white',
        //设置标题的大小
        fontSize: 18,
        //居中显示
        alignSelf: 'center',
    },
    submit: {
        borderRadius: 5,
        backgroundColor: '#108EE9',
        padding: 10,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 38
    },
    submitText: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 16,
    },
    link: {
        justifyContent: 'space-between'
    }
    });
