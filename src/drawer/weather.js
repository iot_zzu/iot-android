/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import AnimatedCircleProgress from '../components/AnimatedCircleProgress';
import CircleProgressView from '../components/CircleProgressView'
import {
    View,
    Image,
    StyleSheet,
    Text,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
var Geolocation = require('Geolocation');
export default class weather extends Component {

    static navigationOptions = ({navigation, screenProps}) => ({

        headerTitle: '天气',
        //设置滑动返回的距离
        gestureResponseDistance: {horizontal: 300},

        //是否开启手势滑动返回，android 默认关闭 ios打开
        gesturesEnabled: true,

        //设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题
        headerBackTitle: null,
        //导航栏的样式
        headerStyle: styles.headerStyle,
        //导航栏文字的样式
        headerTitleStyle: styles.headerTitleStyle,
        //返回按钮的颜色
        headerTintColor: 'white',

        //隐藏顶部导航栏
        // header: null,

        //设置顶部导航栏右边的视图  和 解决当有返回箭头时，文字不居中
        headerRight: (<View/>),

        //设置导航栏左边的视图
        // headerLeft: (<View/>),

    });

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            url:'',
            progress: 90 / 100 * 360,
            callBackProgress: 0,
            update: true,
            longitude:0,
            latitude:0,
            date:[],
            high:[],
            low:[],
            day:[],
            text:[],
            future:[],
            everyday:[],
            Weather:[],
            daydata:[],
            now:[],
            today:[],
            result:[],
            sk:[],
            temp:'',
            temperature:'',
            futemperature:[],
            week:[],
            todayweather:'',
            dressing_index:'',
            ft1:'',
            Adate:[],
            m:[],
        };
        this.state2 = {
            progress: 90 / 100 * 360,
            callBackProgress: 0,
            update: true,
        };
    }
    getLocation() {
        Geolocation.getCurrentPosition(
            location => {
                // let lat=location.coords.latitude;
                // let lon=location.coords.longitude;
                var result = "速度：" + location.coords.speed +
                    "\n经度：" + location.coords.longitude +
                    "\n纬度：" + location.coords.latitude +
                    "\n准确度：" + location.coords.accuracy +
                    "\n行进方向：" + location.coords.heading +
                    "\n海拔：" + location.coords.altitude +
                    "\n海拔准确度：" + location.coords.altitudeAccuracy +
                    "\n时间戳：" + location.timestamp;
                // ToastAndroid.show(lat+","+lon);
                this.setState({longitude:location.coords.longitude});
                this.setState({latitude:location.coords.latitude});
                this.setState({url:"http://v.juhe.cn/weather/geo?format=2&key=dbad7fb407bf3f4e1c2b07afc801d73f&lon="+this.state.longitude+"&lat="+this.state.latitude})
                // this.setState({url:"http://tj.nineton.cn/Heart/index/all?city=CHHA000000&language=&unit=&aqi=&alarm=&key="});
                fetch(this.state.url)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        console.log(responseJson);
                        this.setState({result:responseJson.result});
                        // console.log(this.state.Weather);
                        // this.setState({result:this.state.Weather.result});
                        this.setState({today:this.state.result.today});
                        this.setState({future:this.state.result.future});
                        console.log(this.state.future);
                        this.setState({sk:this.state.result.sk});
                        this.setState({temp:this.state.sk.temp});
                        this.setState({temperature:this.state.today.temperature});
                        this.setState({todayweather:this.state.today.weather});
                        this.setState({dressing_index:this.state.today.dressing_index});
                        for (var i=0;i<this.state.future.length;i++){
                            this.state.futemperature.push(this.state.future[i].temperature);
                            this.state.week.push(this.state.future[i].week);
                            this.state.date.push(this.state.future[i].date);
                            this.setState({ft1:this.state.futemperature[0]});
                        }
                        for (var j=0;j<this.state.date.length;j++){
                            var datestr=this.state.date[j].substr(5);
                            var month=datestr.substr(0,1);
                            var Day=datestr.substr(1);
                            var Date=month+"月"+Day+"日";
                            this.state.Adate.push(Date);
                            this.state.m.push(this.state.Adate[j]+this.state.week[j])
                            console.log(this.state.m);
                            // this.state.Adate.push()

                        }

                        return responseJson.status;
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                alert(this.state.futemperature);
            },
            error => {
                alert("获取位置失败："+ error.data)
            },


        );
    }
    onSuccess(e) {
        // Linking
        //     .openURL(e.data)
        //     .catch(err => console.error('An error occured', err));
        // this.getLocation();
        console.log(e.data);
        // Alert.alert(e.data);
    }

    //改变进度条的长度
    changeProgress() {
        this.setState({
            progress: 97 / 100 * 360,
        });
    }

    componentWillMount() {
        

        // this.getLocation();
    }
    componentDidMount()
    {
        this.getLocation();
        // console.log(this.state.week[0])
        // fetch(this.state.url)
        //     .then((response) => response.json())
        //     .then((responseJson) => {
        //         console.log(responseJson);
        //         // this.setState({message:responseJson.content.username});
        //         return responseJson.status;
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });


    }

    render() {
        // console.log(this.state.future);
        return (
            <View style={styles.container}>
                <ImageBackground style={{width:'100%',height:'100%'}} source={require('../weatherimg/weather_background.jpg')}>
                    <View style={styles.title}>

                    </View>
                    <ScrollView style={styles.scrollView}>
                        <View style={styles.dataShow1}>
                            <Text style={{fontSize:70,textAlign:'center',color:'#fff'}}>
                                {this.state.temp}℃
                            </Text>
                            <Text style={{fontSize:30,textAlign:'center',color:'#fff'}}>
                                {this.state.todayweather}
                            </Text>
                            <Text style={{fontSize:20,textAlign:'center',color:'#fff'}}>
                                {this.state.temperature}
                            </Text>
                            <Text style={{fontSize:20,textAlign:'center',color:'#fff'}}>
                                {this.state.dressing_index}
                            </Text>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        {/*未来10小时天气预报*/}
                        {/*<ScrollView style={styles.hourWeatherView} horizontal={true} showsHorizontalScrollIndicator={false}>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.hourWeather}>*/}
                                {/*<Text style={styles.textStyle}>现在</Text>*/}
                                {/*<Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>*/}
                                {/*<Text style={styles.textStyle}>24℃</Text>*/}
                            {/*</View>*/}
                        {/*</ScrollView>*/}
                        {/*未来7天天气预报*/}
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[0]+this.state.week[0]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[0]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[1]+this.state.week[1]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[1]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[2]+this.state.week[2]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[2]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[3]+this.state.week[3]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[3]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[4]+this.state.week[4]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[4]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[5]+this.state.week[5]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[5]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={styles.dayWeatherView}>
                            <View style={styles.dayWeather}>
                                <Text style={styles.textStyle}>{this.state.Adate[6]+this.state.week[6]}</Text>
                                <Image style={styles.iconStyle} source={require('../weatherimg/sun.png')}/>
                                <Text style={styles.textStyle}>{this.state.futemperature[6]}</Text>
                            </View>
                        </View>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        <View style={{height:1,marginRight:10,marginLeft:10,backgroundColor:'#fff'}}/>
                        {/*空气质量*/}
                        <View style={{height:200,width:'100%',flexDirection:'column',margin:10,}}>
                            <Text style={{fontSize:18,color:'#fff'}}>空气质量</Text>
                            <View style={{height:185,width:'100%',flexDirection:'row'}}>
                                <View style={{height:185,width:'50%',
                                    flexDirection:'column',
                                    justifyContent:'center',
                                    alignItems:'center'}}>
                                    <AnimatedCircleProgress
                                        progressWidth={8}
                                        raduis={60}
                                        progressColor={'#21c7f2'}
                                        baseProgressWidth={8}
                                        progress={this.state.progress}>
                                        <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                            <Text style={{fontSize:15,color:'#fff'}}>
                                                良
                                            </Text>
                                            <Text style={{fontSize:25,color:'#fff'}}>
                                                90
                                            </Text>
                                        </View>
                                    </AnimatedCircleProgress>
                                </View>
                                <View style={{height:185,width:'50%',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                    <Text style={styles.textStyle}>PM10  69</Text>
                                    <Text style={styles.textStyle}>P2.5  44</Text>
                                    <Text style={styles.textStyle}>NO2   20</Text>
                                    <Text style={styles.textStyle}>SO2   11</Text>
                                    <Text style={styles.textStyle}>O3    160</Text>
                                    <Text style={styles.textStyle}>CO    1</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{height:1,margin:10,backgroundColor:'#fff'}}/>
                        {/*舒适度*/}
                        <View style={{height:200,width:'100%',flexDirection:'column',marginRight:10,marginLeft:10,}}>
                            <Text style={{fontSize:18,color:'#fff'}}>舒适度</Text>
                            <View style={{height:185,width:'100%',flexDirection:'row'}}>
                                <View style={{height:185,width:'50%',
                                    flexDirection:'column',
                                    justifyContent:'center',
                                    alignItems:'center'}}>
                                    <AnimatedCircleProgress
                                        progressWidth={8}
                                        raduis={60}
                                        progressColor={'#21c7f2'}
                                        baseProgressWidth={8}
                                        progress={this.state.progress}>
                                        <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                            <Text style={{fontSize:15,color:'#fff'}}>
                                                良
                                            </Text>
                                            <Text style={{fontSize:25,color:'#fff'}}>
                                                90
                                            </Text>
                                        </View>
                                    </AnimatedCircleProgress>
                                </View>
                                <View style={{height:185,width:'50%',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                    <Text style={styles.textStyle}>紫外线指数 1 最弱</Text>
                                    <Text style={styles.textStyle}>体感温度    24℃</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{height:1,margin:10,backgroundColor:'#fff',marginBottom:30}}/>
                    </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    scrollView: {
        width:'100%',
        height:'100%',
        flexDirection:'column',
    },
    dataShow1:{
        height:400,
        width:'100%',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    hourWeatherView:{
        width:'100%',
        height:130,
        flexDirection:'row',
    },
    hourWeather:{
        height:'100%',
        width:60,
        flexDirection:'column',
        justifyContent:'space-around',
        alignItems:'center'
    },
    dayWeatherView:{
        height:50,
        width:'100%',
        flexDirection:'column',
        justifyContent:'space-around',
        alignItems:'center'
    },
    dayWeather:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center'
    },
    textStyle:{
        fontSize:15,
        color:'#fff',
        margin:5
    },
    iconStyle:{
        height:20,
        width:20,
    },
    headerStyle: {
        backgroundColor: '#292D33',
    },
});
