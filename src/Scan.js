import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    CheckBox,
    TouchableOpacity,
    Linking,
    Alert,
    ToastAndroid
} from 'react-native';
// import { QRScannerView } from 'ac-qrcode';
// import Styles from './styles/QQBrowserScreenStyles';
// import {Images,Colors} from "./resource"
// import {ImageButton} from "./components/";
import Toast from 'react-native-simple-toast';
import QRCodeScanner from 'react-native-qrcode-scanner';
var Geolocation = require('Geolocation');
type Props = {};


export default class Scan extends Component{
    constructor(props){
        super(props);
        this.state={
            LocalPosition:'',
        }
    };
    postRequest(){

    }
    getLocation() {
        Geolocation.getCurrentPosition(
            location => {
                let lat=location.coords.latitude;
                let lon=location.coords.longitude;
                var result = "速度：" + location.coords.speed +
                    "\n经度：" + location.coords.longitude +
                    "\n纬度：" + location.coords.latitude +
                    "\n准确度：" + location.coords.accuracy +
                    "\n行进方向：" + location.coords.heading +
                    "\n海拔：" + location.coords.altitude +
                    "\n海拔准确度：" + location.coords.altitudeAccuracy +
                    "\n时间戳：" + location.timestamp;
                // ToastAndroid.show(lat+","+lon);
                alert(result);
                fetch("http://192.168.167.179:8081/iot-admin/eventData/9f8c07ae-b8ba-4f50-bf59-e3f319d99977/locations",{
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        eventDate: "2018-06-02 03:58:00",
                        latLong: [
                            {
                                latitude: location.coords.latitude,
                                longitude: location.coords.longitude
                            }
                        ]
                    })

                }).then((response)=>{
                    console.log(response);
                    this.props.navigation.navigate('MakeInternet',{key:'MakeInternet'})
                }).catch((error)=>{
                    console.error(error);
                })
            },
            error => {
                alert("获取位置失败："+ error)
            },


        );
    }
    onSuccess(e) {
        // Linking
        //     .openURL(e.data)
        //     .catch(err => console.error('An error occured', err));
        this.getLocation();
        console.log(e.data);
        // fetch("http://192.168.167.179:8081/iot-admin/eventData/9f8c07ae-b8ba-4f50-bf59-e3f319d99977/locations",{
        //     method: 'POST',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         eventDate: "2018-06-02 03:58:00",
        //
        //     })
        //
        // }).then((response)=>{
        //     console.log(response);
        //     this.props.navigation.navigate('MakeInternet',{key:'MakeInternet'})
        // }).catch((error)=>{
        //     console.error(error);
        // })
    }
    static navigationOptions = ({navigation, screenProps}) => ({

        headerTitle: '扫码绑定',
        //设置滑动返回的距离
        gestureResponseDistance: {horizontal: 300},

        //是否开启手势滑动返回，android 默认关闭 ios打开
        gesturesEnabled: true,

        //设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题
        headerBackTitle: null,
        //导航栏的样式
        headerStyle: styles.headerStyle,
        //导航栏文字的样式
        headerTitleStyle: styles.headerTitleStyle,
        //返回按钮的颜色
        headerTintColor: 'white',

        //隐藏顶部导航栏
        // header: null,

        //设置顶部导航栏右边的视图  和 解决当有返回箭头时，文字不居中
        headerRight: (<View/>),

        //设置导航栏左边的视图
        // headerLeft: (<View/>),

    });

    render() {
        return (
            <QRCodeScanner
                onRead={this.onSuccess.bind(this)}
                style={styles.scan}
                 topContent={
                     <Text style={{fontSize:30}}>
                         请将二维码置于框内
                     </Text>
                 }
                 bottomContent={
                     <TouchableOpacity style={styles.buttonTouchable}
                                       onPress={()=>{
                                           Alert.alert('暂时还不能反馈');
                                       }
                                       }>
                         <Text style={styles.buttonText}>扫码有问题点击反馈</Text>
                     </TouchableOpacity>

                 }
            />
        );

    }


}
const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    scan:{
        marginLeft:10,
        marginRight:10,
    },
    main:{
        width:'70%',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center'
    },
    inputStyle:{
        width:'100%',
        borderWidth:1,
        borderRadius:5,
        borderColor:'#dbdbdb',
    },
    button:{
        width:'50%',
        height:40,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#2255FF'
    },
    headerStyle: {
        backgroundColor: '#292D33',
    },
    headerTitleStyle: {
        color: 'white',
        //设置标题的大小
        fontSize: 18,
        //居中显示
        alignSelf: 'center',
    },
});
