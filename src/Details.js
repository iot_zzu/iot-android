/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Button,
    TouchableNativeFeedback,
    Alert,
    ToastAndroid,
    ImageBackground,
    ProgressBarAndroid,

} from 'react-native';
import { MapView ,Marker} from 'react-native-amap3d';
// import AMapLocation from 'react-native-smart-amap-location';
// import AppEventListenerEnhance from 'react-native-smart-app-event-listener-enhance'


export default class Details extends Component {

    static navigationOptions = ({navigation, screenProps}) => ({

        headerTitle: '首页',
        //设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题
        // headerBackTite: null,
        //顶部标题栏的样式
        headerStyle: styles.headerStyle,
        //顶部标题栏文字的样式
        headerTitleStyle: styles.headerTitleStyle,
    });
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            message:'123',
            pm2_5:'',
            pm10:'',
            result:[],
            quality:'',
            aqiinfo:[],


        };
    }
    //
    // componentDidMount() {
    //     let viewAppearCallBack = (event) => {
    //         AMapLocation.init(null) //使用默认定位配置
    //     }
    //     this.addAppEventListener(
    //         this.props.navigator.navigationContext.addListener('didfocus', viewAppearCallBack),
    //         NativeAppEventEmitter.addListener('amap.location.onLocationResult', this._onLocationResult)
    //     )
    // }
    //
    // componentWillUnmount () {
    //     //停止并销毁定位服务
    //     AMapLocation.cleanUp()
    // }

    componentDidMount()
    {
        fetch('http://api.jisuapi.com/aqi/query?appkey=4b8a52ae2e26881b&city=郑州',{
            method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',

                },
                //     body: JSON.stringify({
                //     account: 'sck',
                //     password: 'sck',
                // }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({result:responseJson.result});
                this.setState({pm2_5:this.state.result.pm2_5});
                this.setState({pm10:this.state.result.pm10});
                this.setState({quality:this.state.result.quality});
                this.setState({aqiinfo:this.state.result.aqiinfo});
                console.log(this.state.result.pm2_5)
                return responseJson.status;
            })
            .catch((error) => {
                console.error(error);
            });


    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.pageStyle}>
                    <ImageBackground
                        style={styles.img}
                        source={require('../images/test.png')}
                    >
                        <Text style={{color:'white',fontSize:60,fontWeight: 'bold', alignSelf: 'center',marginTop:60}}>{this.state.pm2_5}</Text>
                        <Text style={{color:'white',fontSize:10,fontWeight: 'bold', alignSelf: 'center',marginTop:5}}>空气状况：{this.state.quality}</Text>
                        <Text style={{color:'white',fontSize:10,fontWeight: 'bold', alignSelf: 'center',marginTop:10}}>健康建议：极少数异常敏感人群应减少户外活动</Text>
                    </ImageBackground>
                </View>
                <View style={{flexDirection :'row',borderWidth:1,width:'100%'}}>
                    <Text style={{marginLeft:5}}>AQI:0[优]</Text>
                    <Text style={{marginLeft:170}}>主要污染物：pm2.5</Text>
                </View>
                <View style={{width:'100%',height: 120,backgroundColor:'#EDEDED',flexDirection:'column'}}>
                    <View style={{width:'100%',backgroundColor:'#EDEDED',flexDirection:'column'}}>
                        <Text>（温度为℃）（湿度为RH%）（pm2.5，pm10为ug/m³）</Text>

                    </View>

                    <View style={{flexDirection:'row',height:'80%',backgroundColor:'#EDEDED',marginLeft:1}}>
                        <TouchableNativeFeedback
                            activeOpacity={0.5}
                            onPress={()=>{
                                this.props.navigation.navigate('pmtw',{key: 'pmtw'})
                            }}
                            // onPress={()=> {
                            //     Alert.alert('提示','pm2.5',[
                            //         {text:'取消',onPress:()=>{
                            //             ToastAndroid.show('点击了取消',ToastAndroid.SHORT)},style:'cancel'},
                            //         {text:'确认',onPress:()=>{ToastAndroid.show('点击了确认',ToastAndroid.SHORT)}},
                            //     ])
                            // }}
                        >

                            <View style={{flexDirection:'column',width:'22%', margin:5,backgroundColor:'white',flexDirection:'column'}}>
                                <Text style={{alignSelf:'center',marginTop:15,fontSize:15}}>pm2.5</Text>
                                <View style={styles.ViewForTextStyle}>

                                    <Text style={{backgroundColor:'#8BC34A',alignSelf:'center', borderRadius: 30,
                                        width:50,height:30,textAlign:'center',justifyContent: 'center',}}>12</Text>
                                </View>


                                {/*<Button defaultValue={'12'} title='12' style={styles.button}></Button>*/}

                            </View>
                        </TouchableNativeFeedback>
                        {/*----------------------------------------------------------------------*/}
                        <TouchableNativeFeedback
                            activeOpacity={0.5}
                            onPress={()=>{
                                this.props.navigation.navigate('pmten',{key: 'pmten'})
                            }}
                        >
                            <View style={{flexDirection:'column',width:'22%', margin:5,backgroundColor:'white',flexDirection:'column'}}>
                                <Text style={{alignSelf:'center',marginTop:15,fontSize:15}}>pm10</Text>
                                {/*<Button defaultValue={'12'} title='12' style={styles.button}></Button>*/}
                                <View style={styles.ViewForTextStyle}>

                                    <Text style={{backgroundColor:'#8BC34A',alignSelf:'center', borderRadius: 30,
                                        width:50,height:30,textAlign:'center',justifyContent: 'center',}}>12</Text>
                                </View>


                            </View>
                        </TouchableNativeFeedback>
                        {/*-----------------------------------------------------------------------*/}
                        <TouchableNativeFeedback
                            activeOpacity={0.5}
                            onPress={()=>{
                                this.props.navigation.navigate('temp',{key: 'temp'})
                            }}
                            // onPress={()=> {
                            //     Alert.alert('提示','温度',[
                            //         {text:'取消',onPress:()=>{
                            //             ToastAndroid.show('点击了取消',ToastAndroid.SHORT)},style:'cancel'},
                            //         {text:'确认',onPress:()=>{ToastAndroid.show('点击了确认',ToastAndroid.SHORT)}},
                            //     ])
                            // }}
                        >
                            <View style={{flexDirection:'column',width:'22%', margin:5,backgroundColor:'white',flexDirection:'column'}}>
                                <Text style={{alignSelf:'center',marginTop:15,fontSize:15}}>温度</Text>
                                {/*<Button defaultValue={'12'} title='12'style={styles.button}></Button>*/}
                                <View style={styles.ViewForTextStyle}>

                                    <Text style={{backgroundColor:'#8BC34A',alignSelf:'center', borderRadius: 30,
                                        width:50,height:30,textAlign:'center',justifyContent: 'center',}}>12</Text>
                                </View>


                            </View>
                        </TouchableNativeFeedback>
                        {/*-----------------------------------------------------------------------*/}
                        <TouchableNativeFeedback
                            activeOpacity={0.5}
                            onPress={()=>{
                                this.props.navigation.navigate('humidity',{key: 'humidity'})
                            }}
                            // onPress={()=> {
                            //     Alert.alert('提示','湿度',[
                            //         {text:'取消',onPress:()=>{
                            //             ToastAndroid.show('点击了取消',ToastAndroid.SHORT)},style:'cancel'},
                            //         {text:'确认',onPress:()=>{ToastAndroid.show('点击了确认',ToastAndroid.SHORT)}},
                            //     ])
                            // }}
                        >
                            <View style={{flexDirection:'column',width:'22%', margin:5,backgroundColor:'white',flexDirection:'column'}}>
                                <Text style={{alignSelf:'center',marginTop:15,fontSize:15}}>湿度</Text>
                                {/*<Button defaultValue={'12'} title='12' style={styles.button}></Button>*/}
                                <View style={styles.ViewForTextStyle}>

                                    <Text style={{backgroundColor:'#8BC34A',alignSelf:'center', borderRadius: 30,
                                        width:50,height:30,textAlign:'center',justifyContent: 'center',}}>12</Text>
                                </View>


                            </View>
                        </TouchableNativeFeedback>
                        {/*-----------------------------------------------------------------------*/}

                    </View>
                </View>
                <View
                    style={{height:340,width:'100%'}}
                >

                    <MapView
                        // Region={{width:100,height:100}}
                        //不是全屏不能显示
                        style={{height:260,width:'100%'}}

                        coordinate={{
                            latitude: 34.750923,
                            longitude: 113.699744,
                        }}
                        zoomLevel={12}
                        tilt={45}
                        showsIndoorMap
                        onPress={()=>{
                            this.props.navigation.navigate('pmmap',{key: '1234'})
                        }}
                    >
                        <Marker
                            active
                            image='point'
                            title='雾霾盒子点'
                            color='red'
                            description='没有数据'
                            coordinate={{

                                latitude: 34.750623,
                                longitude: 113.699744,
                            }}
                        />
                        <Marker
                            active
                            image='point'
                            title='雾霾盒子点'
                            color='red'
                            description='没有数据'
                            coordinate={{

                                latitude: 34.750750,
                                longitude: 113.699744,
                            }}
                        />
                        <Marker
                            active
                            image='point'
                            title='雾霾盒子点'
                            color='red'
                            description='没有数据'
                            coordinate={{

                                latitude: 34.750840,
                                longitude: 113.699744,
                            }}
                        />
                        <Marker
                            active
                            image='point'
                            title='雾霾盒子点'
                            color='red'
                            description='没有数据'
                            coordinate={{

                                latitude: 34.750130,
                                longitude: 113.699744,
                            }}
                        />

                    </MapView>
                </View>

            </View>



        );
    }
    // _onLocationResult = (result) => {
    //     if(result.error) {
    //         Alert.alert(`错误代码: ${result.error.code}, 错误信息: ${result.error.localizedDescription}`)
    //     }
    //     else {
    //         if(result.formattedAddress) {
    //             Alert.alert(`格式化地址 = ${result.formattedAddress}`)
    //         }
    //         else {
    //             Alert.alert(`纬度 = ${result.coordinate.latitude}, 经度 = ${result.coordinate.longitude}`)
    //         }
    //     }
    //     if(this._button_1.state.loading) {
    //         this._button_1.setState({
    //             loading: false,
    //         })
    //     }
    //     if(this._button_2.state.loading) {
    //         this._button_2.setState({
    //             loading: false,
    //         })
    //     }
    // }
    //
    // //单次定位并返回逆地理编码信息
    // _showReGeocode = () => {
    //     this._button_1.setState({
    //         loading: true,
    //     })
    //     AMapLocation.getReGeocode()
    // }
    //
    // //单次定位并返回地理编码信息
    // _showLocation = () => {
    //     this._button_2.setState({
    //         loading: true,
    //     })
    //     AMapLocation.getLocation()
    // }
    //
    // _renderActivityIndicator() {
    //     return ActivityIndicator ? (
    //         <ActivityIndicator
    //             style={{margin: 10,}}
    //             animating={true}
    //             color={'#fff'}
    //             size={'small'}/>
    //     ) : Platform.OS == 'android' ?
    //         (
    //             <ProgressBarAndroid
    //                 style={{margin: 10,}}
    //                 color={'#fff'}
    //                 styleAttr={'Small'}/>
    //
    //         ) :  (
    //             <ActivityIndicatorIOS
    //                 style={{margin: 10,}}
    //                 animating={true}
    //                 color={'#fff'}
    //                 size={'small'}/>
    //         )
    // }
}
// export default AppEventListenerEnhance(Details)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height:100,


    },
    img:{
        flexDirection:'column',
        width:500,
        height:200
    },
    text1:{
        color: 'black',
    },
    pageStyle: {
        flexDirection:'column',
        alignItems: 'stretch',
        width:500,
        height: 200,
        backgroundColor:'red'
    },

    button: {
        width: 240,
        height: 45,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#8BC34A',
    },
    headerStyle: {
        backgroundColor: '#292D33',
    },
    headerTitleStyle: {
        //标题的文字颜色
        color: 'white',
        //设置标题的大小
        fontSize: 18,
        //居中显示
        alignSelf: 'center',
    },
    ViewForTextStyle:{
        borderRadius: 30,
        alignSelf: 'center',
        height:30,
        width:50,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor:'#8BC34A',
        margin:5
    },
    containers: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },

});
